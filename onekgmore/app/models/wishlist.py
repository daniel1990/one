from django.db import models
from django.contrib.auth.models import User
from .project import Project


class Wishlist(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    items = models.CharField('Wishlist', max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    # user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)

    def __unicode__(self):
        return self.items


# class WishlistItem(models.Model):
#     item = models.CharField('Wishlist Item', max_length=50)
#     quantity = models.IntegerField('Item Quantity', blank=True)
#     dimensions_unit = models.CharField('Dimensions Unit', max_length=20, blank=True)
#     width = models.FloatField(blank=True)
#     length = models.FloatField(blank=True)
#     height = models.FloatField(blank=True)
#     weight = models.FloatField(blank=True)
#     weight_unit = models.CharField('Weight Unit', max_length=20, blank=True)
#     wishlist = models.ForeignKey(Wishlist, on_delete=models.CASCADE)

#     def __str__(self):
#         return self.item