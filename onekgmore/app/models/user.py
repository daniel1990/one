from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from slugify import slugify

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bio = models.TextField('Bio', blank=True)
    name_slug = models.SlugField(max_length=140, unique=True)
    website = models.URLField('Website', default="", blank=True)
    # avatar = models.ImageField(upload_to='profile_images/%Y/%m/%d', blank=True)

    def __str__(self):
        return self.user.first_name + " " + self.user.last_name

    def _get_unique_slug(self):
        name_slug = slugify(self.user.first_name + " " + self.user.last_name)
        unique_slug = name_slug
        num = 1
        while Profile.objects.filter(name_slug=unique_slug).exists():
            unique_slug = '{}-{}'.format(name_slug, num)
            num += 1
        return unique_slug
 
    def save(self, *args, **kwargs):
        if not self.name_slug:
            self.name_slug = self._get_unique_slug()
        super().save(*args, **kwargs)


def create_profile(sender, **kwargs):
    user = kwargs['instance']
    if kwargs['created']:
        user_profile = Profile(user=user)
        user_profile.save()
    
post_save.connect(create_profile, sender=User)