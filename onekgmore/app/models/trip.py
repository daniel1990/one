from django.db import models
from django.contrib.auth.models import User
from .project import Project


class Trip(models.Model):
    date = models.DateTimeField('Date', blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    project = models.ForeignKey(Project, verbose_name='Project Name', on_delete=models.CASCADE)
    user = models.ForeignKey(User, verbose_name='Traveler', on_delete=models.CASCADE)

    def __str__(self):
        return self.project.name + ' on ' + str(self.date)
