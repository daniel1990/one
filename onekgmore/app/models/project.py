from django.db import models
from django.contrib.auth.models import User
from .organization import NonProfitOrganization
from .address import Address



class Project(models.Model):
    name = models.CharField('Name', max_length=255)
    description = models.TextField('Description', blank=True)
    is_completed = models.BooleanField('Project Completed', default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    address = models.ForeignKey(Address, verbose_name='Address', null=True, on_delete=models.SET_NULL)
    organization = models.ForeignKey(NonProfitOrganization, verbose_name='Organization', related_name='projects', on_delete=models.CASCADE)

    class Meta:
        ordering = ['-updated_at']

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return '/project/%s/' % self.id
