from django.db import models
from django_countries.fields import CountryField

class Address(models.Model):
	address_line_1 = models.CharField('Address Line 1', max_length=200)
	address_line_2 = models.CharField('Address Line 2', max_length=200, blank=True)
	address_line_3 = models.CharField('Address Line 3', max_length=200, blank=True)
	city = models.CharField('City', max_length=50)
	state = models.CharField('State or Province', max_length=50, blank=True)
	postal_code = models.CharField('Postal Code', max_length=50, blank=True)
	country = CountryField(default='US')

	class Meta:
		verbose_name_plural = "addresses"

	def get_full_address(self):
		full_address = ''
		if self.address_line_1:
			full_address += self.address_line_1 + ', '
		if self.address_line_2:
			full_address += self.address_line_2 + ', '
		if self.address_line_3:
			full_address += self.address_line_3 + ', '
		if self.city:
			full_address += self.city + ', '
		if self.state:
			full_address += self.state + ', '
		if self.postal_code:
			full_address += self.zipcode + ', '
		if self.country:
			full_address += str(self.country.name)
		return full_address