from django.db import models
from django.contrib.auth.models import User
from .address import Address
from slugify import slugify

orgCategories = (
  ('animal','Animal Welfare'),
  ('arts-culture','Arts and Culture'),
  ('children','Children'),
  ('civil-rights','Civil Rights'),
  ('climate-change','Climate Change'),
  ('disaster-relief','Disaster Relief'),
  ('economic-development','Economic Development'),
  ('education','Education'),
  ('environment','Environment'),
  ('health','Health'),
  ('hiv-aids','HIV-AIDS'),
  ('human-rights','Human Rights'),
  ('hunger','Hunger'),
  ('poverty','Poverty'),
  ('science-technology','Science and Technology'),
  ('social-services','Social Services'),
  ('women-girls','Women and Girls'),
  ('other','Other')
)


class NonProfitOrganization(models.Model):
    name = models.CharField('Name', max_length=120)
    name_slug = models.SlugField(max_length=140, unique=True)
    bridge_number = models.CharField('Bridge Number', db_index=True, max_length=70)
    ein = models.CharField('EIN', null=True, max_length=70)
    about = models.TextField('About', blank=True)
    description = models.TextField('Description', blank=True)
    email = models.EmailField('Email')
    url = models.URLField('URL', default="", blank=True)
    phone = models.CharField('Phone', max_length=70)
    # logo = models.ImageField(upload_to='org_images/%Y/%m/%d', blank=True)
    is_faith_based = models.BooleanField('Faith Based', default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_locked = models.BooleanField('Organization Locked', default=True)
    category = models.CharField('Category', max_length=255, choices = orgCategories)
    # user = models.ForeignKey(User, verbose_name='Created By', blank=True, null=True)
    address = models.ForeignKey(Address, verbose_name='Address', null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.name

    def _get_unique_slug(self):
        name_slug = slugify(self.name)
        unique_slug = name_slug
        num = 1
        while NonProfitOrganization.objects.filter(name_slug=unique_slug).exists():
            unique_slug = '{}-{}'.format(name_slug, num)
            num += 1
        return unique_slug
 
    def save(self, *args, **kwargs):
        if not self.name_slug:
            self.name_slug = self._get_unique_slug()
        super().save(*args, **kwargs)

    def get_absolute_url(self):
            return '/organization/%s/' % self.name_slug
