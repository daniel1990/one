from django.shortcuts import render
from django.http import HttpResponse
from django.core import validators
from rest_framework.generics import ListAPIView
from app.api.serializers import TripSerializer


def index(request):
    return HttpResponse("Hello, world. You're at the 1KG More App.")


class TripList(ListAPIView):
    serializer_class = TripSerializer
    validator_map = {
        'organization_id': validators.integer_validator,
        'project_id': validators.integer_validator,
        # 'created_at': is_valid_date,
        # 'updated_at': is_valid_date,
        # 'sort': optional_sort_parameter_validator_factory(TrafficSerializer.Meta.fields),
    }

    def get_trip_params(self):
        return {
            'organization_id': self.kwargs['user_id'],
            'project_id': self.kwargs['site_id'],
            # 'start_date': self.request.query_params.get('start_date', None),
            # 'end_date': self.request.query_params.get('end_date', None),
            # 'filter': self.request.query_params.get('filter', None),
            # 'per_page': self.request.query_params.get('per_page', None),
            # 'sort': self.request.query_params.get('sort', None),
        }

    def validate(self, request, *args, **kwargs):
        params = self.get_traffic_params()
        errors = {}
        for key, validator in self.validator_map.items():
            try:
                validator(params[key])
            except ValidationError as e:
                errors[key] = e.message
        if len(errors) > 0:
            return response.HttpResponseBadRequest(JSONRenderer().render(errors))
        return None

    def get_queryset(self):
        if self.queryset is not None:
            return self.queryset
        params = self.get_traffic_params()
        result = self._get_queryset(params)
        if params['filter'] is not None and len(params['filter']) > 0:
            result = filter_url(params['filter'], result, False)
        if params['sort'] is not None and len(params['sort']) > 0:
            result = sort_by_parameter(params['sort'], result)
        return result

    def _get_queryset(self, params: dict):
        cache_key = 'QUERY:TRAFFIC:' + '_'.join([y for x, y in params.items() if x in self.elasticsearch_params])
        nonuniq = cache.get(cache_key) if cache_key in cache else self._query_elasticsearch(params)
        uniq = AggSumByDay.agg_sum_page_by_day(nonuniq)
        cache.set(cache_key, uniq)
        return uniq
