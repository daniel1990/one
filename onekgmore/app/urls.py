from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('organizations/<int:bridge_number>/projects/<int:id>/trips', views.TripList.as_view()),
    # path('organizations/<int:bridge_number>/projects/<int:id>', views.PageGroupedTrafficList.as_view()),
]