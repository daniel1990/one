from django.contrib import admin
from app.models.organization import NonProfitOrganization
from app.models.project import Project
from app.models.address import Address
from app.models.trip import Trip
from app.models.user import Profile
from app.models.wishlist import Wishlist

admin.site.register(NonProfitOrganization)
admin.site.register(Project)
admin.site.register(Address)
admin.site.register(Profile)
admin.site.register(Trip)
admin.site.register(Wishlist)