from rest_framework import serializers
from django.contrib.auth.models import User
from app.models.organization import *
from app.models.user import *
from app.models.project import *
from app.models.wishlist import *
from app.models.trip import *
from app.models.address import *


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer(many=False)

    class Meta:
        model = User
        fields = ('pk','username', 'email', 'first_name', 'last_name', 'is_staff', 'is_active', 'date_joined', 'profile')
        read_only_fields = ('pk', 'date_joined')


class WishlistSerializer(serializers.ModelSerializer):
    class Meta:
        model = Wishlist
        fields = '__all__'


class TripSerializer(serializers.ModelSerializer):
    class Meta:
        model = Trip
        fields = '__all__'

        
class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = '__all__'


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = '__all__'


class NonProfitOrganizationSerializer(serializers.ModelSerializer):
    projects = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    addresses = AddressSerializer(many=True, read_only=True)
    
    class Meta:
        model = NonProfitOrganization
        fields = '__all__'
        depth = 2
