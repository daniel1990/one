from rest_framework import permissions, viewsets, filters
from .serializers import *
from .permissions import *
from app.models.organization import *
from app.models.user import *
from app.models.project import *
from app.models.wishlist import *
from app.models.trip import *
from app.models.address import *
import django_filters

class NonProfitOrganizationsViewSet(viewsets.ModelViewSet):
    serializer_class = NonProfitOrganizationSerializer
    queryset = NonProfitOrganization.objects.all()
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly,
    ]

class ProjectsViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectSerializer
    queryset = Project.objects.all()
    filter_fields = ('organization',)
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly,
    ]


class UsersViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = [
        permissions.IsAuthenticated,
        IsUserOrReadOnly,
    ]

class TripsViewSet(viewsets.ModelViewSet):
    serializer_class = TripSerializer
    queryset = Trip.objects.all()
    filter_fields = ('project', 'user',)
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly,
    ]

class WishlistViewSet(viewsets.ModelViewSet):
    serializer_class = WishlistSerializer
    queryset = Wishlist.objects.all()
    filter_fields = ('project', 'user',)
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly,
    ]
