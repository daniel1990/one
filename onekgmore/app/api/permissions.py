from rest_framework import permissions

class SafeMethodsOnlyPermission(permissions.BasePermission):
    """Only can access non-destructive methods (like GET and HEAD)"""
    def has_permission(self, request, view):
        return self.has_object_permission(request, view)

    def has_object_permission(self, request, view, obj=None):
        return request.method in permissions.SAFE_METHODS


class IsUserOrReadOnly(SafeMethodsOnlyPermission):
    """Object-level permission to only allow user to edit him/herself"""
    def has_object_permission(self, request, view, obj=None):
        if obj is None:
            # Either a list or a create, so no admin
            can_edit = True
        else:
            can_edit = request.user == obj
        return can_edit or super(IsUserOrReadOnly, self).has_object_permission(request, view, obj)
