# 1KG More

Powered by Django *(2.0.7 with Python 3.7)*, Django REST Framework.

## Introduction

[1KG More](http://1kgmore.org) is a simple tool that allows nonprofit organizations to post project destinations, along with a list of items that could be used or are needed for the project. Travelers who happen to travel to the same destination can sign up and choose how much and what they are willing to carry (for the organization), starting at 1kg.

## Installation

The application can be launched manually with python.

```
python manage.py migrate
python manage.py runserver
```