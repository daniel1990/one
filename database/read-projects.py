import os, sys
import pandas as pd
import django


df = pd.read_pickle('projects_raw.pkl')

df = df.loc[~(df.bridgeId.isnull())]

projects = df[['title', 'summary', 'ein', 'bridgeId', 'themeName', 'latitude', 'longitude']]

sys.path.append('../onekgmore') # add path to project root dir
os.environ["DJANGO_SETTINGS_MODULE"] = 'onekgmore.settings'

# for more sophisticated setups, if you need to change connection settings (e.g. when using django-environ):
# os.environ["DATABASE_URL"] = "postgres://myuser:mypassword@localhost:54324/mydb"

# Connect to Django ORM
django.setup()

# process data
from app.models.organization import NonProfitOrganization
from app.models.project import Project

for item in df.itertuples():
	organization = NonProfitOrganization.objects.filter(bridge_number=item.bridgeId)
	if organization:
		project = Project.objects.create(name=item.title, description=item.summary, organization=organization[0])
	else:
		pass
		