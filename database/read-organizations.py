import os, sys
import pandas as pd
import django


df = pd.read_pickle('organizations_raw.pkl')

df = df.loc[~(df.bridgeId.isnull())]

organizations = df[['name', 'mission', 'ein', 'bridgeId', 'url', 'addressLine1', 'addressLine2', 'city', 'state', 'postal', 'iso3166CountryCode']]

sys.path.append('../onekgmore') # add path to project root dir
os.environ["DJANGO_SETTINGS_MODULE"] = 'onekgmore.settings'

# for more sophisticated setups, if you need to change connection settings (e.g. when using django-environ):
# os.environ["DATABASE_URL"] = "postgres://myuser:mypassword@localhost:54324/mydb"

# Connect to Django ORM
django.setup()

# process data
from app.models.organization import NonProfitOrganization
from app.models.address import Address

for item in df.itertuples():
	address = Address.objects.create(address_line_1=item.addressLine1, address_line_2=item.addressLine2, city=item.city, state=item.state, postal_code=item.postal, country=item.iso3166CountryCode)
	organization = NonProfitOrganization.objects.create(name=item.name, description=item.mission, ein=item.ein, bridge_number=item.bridgeId, url=item.url, address=address)