from bs4 import BeautifulSoup
import requests

# Scraping packforapurpose.org destination pages for project destinations and wish lists

root_url = 'https://www.packforapurpose.org/destinations/africa/botswana/bush-ways-mobiles/'

headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36'}

# fetch the content from url
page_response = requests.get(root_url, headers=headers, timeout=5)
# while page_response.status_code == 200:
	# try:

# parse html, select the first section as document root, contains all the information we needed
page_content = BeautifulSoup(page_response.content, "html.parser")

# Organization Descriptions
# <div class="project_content project_desc">
# 	<p>African Community Outreach supports hundreds of families, visiting with them in their homes, providing food, providing medical care, and helping to get children into schools.</p>
# </div> 
projects = page_content.find_all('div', attrs={'class':'project_desc'})
# print(projects)

# Wishlists
# <div class="project_content project_needs">
# 	<p><b> General School Supplies: </b><br />
# 	Book Bags, Crayons, Erasers, Pencils, Pens, Rulers, and Solar Calculators</p>
# </div>
wishlists = page_content.find_all('div', attrs={'class':'project_needs'})
# print(wishlists)


# Organization Categories
# <div class="project_content initiatives_items"><a href="https://www.packforapurpose.org/initiatives/health/">Health</a></div>
categories = page_content.find_all('div', attrs={'class':'initiatives_items'})
# print(categories)
